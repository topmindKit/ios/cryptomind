// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "CryptoMind",
        platforms: [
        .macOS(.v10_12), .iOS(.v10), .watchOS(.v4), .tvOS(.v10)
    ],
    products: [        
        .library(name: "CryptoMind", targets: ["CryptoMind"]),
    ],
    dependencies: [
        
    ],
    targets: [
        .target(name: "CryptoMind", dependencies: []),
        .testTarget(name: "CryptoMindTests", dependencies: ["CryptoMind"]),
    ]
)
