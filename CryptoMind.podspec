Pod::Spec.new do |s|
    s.name         = "CryptoMind"
    s.version      = "1.1.1"
    s.summary      = "topmindKit framework"
    s.homepage     = "https://www.topmind.eu"
    s.license      = "All rights reserved topmind GmbH"
    s.authors      = ["Martin Gratzer", "Denis Andrašec"]
    
    s.ios.deployment_target = "10.0"
    s.osx.deployment_target = "10.12"
    s.watchos.deployment_target = "4.0"
    s.tvos.deployment_target = "10.0"  
  
    s.swift_version = "5.0"
    s.source = {
        :git => "https://gitlab.com/topmindKit/ios/coredatamind.git",
        :tag => "#{s.version}"
    }
    s.xcconfig = { 
        'SWIFT_INCLUDE_PATHS' => '$(PODS_ROOT)/CryptoMind/Sources/CommonCrypto'
    }
    s.preserve_paths = 'Sources/CommonCrypto/module.map'
    s.source_files  = [ "Sources/#{s.name}/**/*.{h,m,swift}" ]
    s.exclude_files = [ "Sources/#{s.name}Tests/**/*.{h,m,swift}" ]
    s.resources     = [ "Sources/#{s.name}/**/*.{xib,storyboard,strings,xcassets}" ]
    s.frameworks    = [ 'Foundation' ]   
end
  